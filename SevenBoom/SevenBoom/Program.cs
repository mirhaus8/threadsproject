﻿using System;
using System.Threading;

namespace SevenBoom
{
    class Program
    {
        static void Main(string[] args)
        {
            SharedObject shared = new SharedObject();

            //First Task
            Thread firstThread = new Thread(new ThreadStart(shared.PrintNumOrBoom));
            Thread secondThread = new Thread(new ThreadStart(shared.PrintNumOrBoom));
            Thread thirdThread = new Thread(new ThreadStart(shared.PrintNumOrBoom));
            Thread fourthThread = new Thread(new ThreadStart(shared.PrintNumOrBoom));

            firstThread.Start();
            secondThread.Start();
            thirdThread.Start();
            fourthThread.Start();

            //Second Task
            /*ThreadPool.QueueUserWorkItem(new WaitCallback((object obj) => { shared.PrintNumOrBoom(); }));
            ThreadPool.QueueUserWorkItem(new WaitCallback((object obj) => { shared.PrintNumOrBoom(); }));
            ThreadPool.QueueUserWorkItem(new WaitCallback((object obj) => { shared.PrintNumOrBoom(); }));
            ThreadPool.QueueUserWorkItem(new WaitCallback((object obj) => { shared.PrintNumOrBoom(); }));
            Thread.Sleep(2000);*/
        }
    }
}
