using System;
using System.Collections.Generic;
using System.Text;

namespace SevenBoom
{
    class SharedObject
    {
        private int SharedInt { get; set; }

        private object obj;

        public SharedObject()
        {
            SharedInt = 0;
            obj = new object();
        }

        public void PrintNumOrBoom()
        {
            while (SharedInt <= 200)
            {
                int a;
                lock (obj)
                {
                    a = SharedInt++;
                }
                if (a <= 200)
                {
                    if (a % 7 == 0 || a.ToString().Contains(7.ToString()))
                    { Console.WriteLine("BOOM"); }
                    else
                    { Console.WriteLine(a); }
                }
            }
        }
    }
}
