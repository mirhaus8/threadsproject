﻿using System;
using System.Threading;


namespace PrintingThreads
{
    class Program
    {
        static Object obj = new Object();
        static void Main(string[] args)
        {
            //FirstTask();
            //ThreadsWithParameters();
            //ThreadsSync();
            //ThreadPoolTask();
            //lockingThread();
        }

        static void FirstTask()
        {
            //Thread backgroundThread = new Thread(new ThreadStart(() => { for (int i = 0; i < 100; i++) Console.WriteLine($"{i} - Y"); }));

            Thread workingThread = new Thread(new ThreadStart(() => { for (int i = 0; i < 100; i++) Console.WriteLine($"{i} - X"); }))
            { IsBackground = true };
            workingThread.Start();

            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine($"main {i} - Y");
            }
        }

        static void ThreadsWithParameters()
        {
            Thread tParm = new Thread(new ParameterizedThreadStart((object arg) => { for (int i = 0; i < 5; i++) Console.WriteLine(arg); }));
            Thread tParmSec = new Thread(new ParameterizedThreadStart((object arg) => { for (int i = 0; i < 5; i++) Console.WriteLine(arg); }));
            
            tParm.Start("param");
            tParmSec.Start("param");
        }

        static void ThreadPoolTask()
        {
            ThreadPool.SetMaxThreads(Environment.ProcessorCount, 25);
            ThreadPool.QueueUserWorkItem((object obj) => { Console.WriteLine("first thread"); });
            ThreadPool.QueueUserWorkItem((object obj) => { Console.WriteLine("second thread"); });
            Thread.Sleep(1000);
        }

        static void ThreadsSync()
        {
            Thread workingThread = new Thread(new ThreadStart(() => { for (int i = 0; i < 100; i++) Console.WriteLine($"{i} - X"); Thread.Sleep(100); }));
            
            workingThread.Start();
            workingThread.Join();
            Thread.Sleep(2000);
            Console.WriteLine("Thread ended printing");

        }

        static void lockingThread()
        {
            Thread workingThread = new Thread(new ThreadStart(() => { lock (obj) { for (int i = 0; i < 20; i++) { Console.WriteLine("first thread"); } } })); 
            Thread workingThreadSec = new Thread(new ThreadStart(() => { lock (obj) { for (int i = 0; i < 20; i++) { Console.WriteLine("second thread"); } } }));
            workingThreadSec.Start();
            workingThread.Start();

        }


    }
}
