﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TaskScheduler
{
    class Timer
    {
        private static Timer mInstance = null;
        public static int mSecondsElapsed;
        private Timer() 
        {
            mSecondsElapsed = 0;
        }

        public static Timer Instance
        {
            get
            {
                if (mInstance == null)
                {
                    mInstance = new Timer();
                }
                return mInstance;
            }
        }

        public void ActivateTimer()
        {
            Thread t = new Thread(()=>
            {
                while (true)
                {
                    mSecondsElapsed++;
                    SecondElapsed?.Invoke();
                    Thread.Sleep(1000);
                }
            });
            t.Start();
            
        }

        public static event Action SecondElapsed;
    }
}
