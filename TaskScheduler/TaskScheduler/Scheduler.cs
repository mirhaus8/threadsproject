﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Timers;

namespace TaskScheduler
{
    class Scheduler<T,V>
    {
        private Dictionary<int, KeyValuePair<Func<T, V>, int>> mTasks;
        private Dictionary<int, T> mParamsList;
        private int mSecondsElapsed;
        private int mNextId;
        private System.Timers.Timer mTimer;

        public Scheduler()
        {
            mTasks = new Dictionary<int, KeyValuePair<Func<T, V>, int>>();
            mParamsList = new Dictionary<int, T>();
            mSecondsElapsed = 0;
            mNextId = 0;
            Timer.SecondElapsed += TimeElapsed;
        }

        public void TimeElapsed(/*object sender, ElapsedEventArgs e*/)
        {
            mSecondsElapsed++;
            foreach(KeyValuePair<int, KeyValuePair<Func<T, V>, int>> task in mTasks)
            {
                if(task.Value.Value == mSecondsElapsed)
                {
                    T param = mParamsList[task.Key];
                    Thread taskThread = new Thread(new ThreadStart(() => task.Value.Key?.Invoke(param)));
                    taskThread.Start();
                }
            }
        }

        /*public void Start()
        {
            mTimer = new System.Timers.Timer(1000);
            mTimer.Elapsed += new ElapsedEventHandler(TimeElapsed);
            mTimer.AutoReset = true;
            mTimer.Enabled = true;
            mTimer.Start();
        }*/

        public void AddTask(Func<T, V> task, int timeToActivate, T param)
        {
            int taskId = mNextId++;
            mTasks.Add(taskId, new KeyValuePair<Func<T, V>, int>(task, timeToActivate));
            mParamsList.Add(taskId, param);
        }

        public void RemoveTask(Func<T, V> taskRef, int timeToActivate)
        {
            foreach(KeyValuePair<int, KeyValuePair<Func<T, V>, int>> task in mTasks)
            {
                if(task.Value.Key == taskRef && task.Value.Value == timeToActivate)
                {
                    if(task.Value.Value < mSecondsElapsed)
                    {
                        Console.WriteLine("Too Late!");
                    }
                    else
                    {
                        mTasks.Remove(task.Key);
                    }
                    break;
                }
            }
        }
    }
}
