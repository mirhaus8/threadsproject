﻿using System;

namespace TaskScheduler
{
    class Program
    {
        static void Main(string[] args)
        {
            Scheduler<string,string> s = new TaskScheduler.Scheduler<string,string>();
            Timer timer = Timer.Instance;
            timer.ActivateTimer();
 
            bool keepAlive = true;
            while (keepAlive)
            {
                Console.WriteLine("1) Add new task to scheduler.\r\n" +
                                  "2) Remove task\r\n" +
                                  "3) End scheduler."); ;
                int userChoice = int.Parse(Console.ReadLine());
                switch (userChoice)
                {
                    case 1:
                        Console.WriteLine("Enter the desired time of the task");
                        int taskTime = int.Parse(Console.ReadLine());
                        Console.WriteLine("Enter the task param");
                        string param = Console.ReadLine();
                        s.AddTask(new Func<string, string>(Ring), taskTime, param);
                        break;
                    case 2:
                        Console.WriteLine("Enter the time of the task you want to remove");
                        int timeOfTaskToRemove = int.Parse(Console.ReadLine());
                        s.RemoveTask(new Func<string, string>(Ring), timeOfTaskToRemove);
                        break;
                    case 3:
                        keepAlive = false;
                        break;
                }
            }
        }

        static string Ring(string str)
        {
            Console.WriteLine(str);
            return str;
        }

        static string RingRing(string str)
        {
            Console.WriteLine(str);
            return str;
        }

        static string RingRingRing(string str)
        {
            Console.WriteLine(str);
            return str;
        }
    }
}
