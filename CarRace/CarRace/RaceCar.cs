using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace CarRace
{
    class RaceCar
    {
        public static string Winner;
        public string Name { get; set; }
        private int mMaxSpeed;
        private int mAcceleration;
        public int Capacity { get; set; }
        public int MaxFuelCapacity { get; set; }
        public int FuelAmountLeft { get; set; }
        public int CurrentPlace { get; set; }
        public int CurrentSpeed { get; set; }
        public int NumOfPassengers { get; set; }
        private int mTrackLength;
        private int mRoundNumber;

        public RaceCar(string name, int speed, int acceleration, int capacity, int maxFuel, int fuelCapacity, int numOfPass, int length)
        {
            Winner ="";
            Name = name;
            mMaxSpeed = speed;
            mAcceleration = acceleration;
            Capacity = capacity;
            MaxFuelCapacity = maxFuel;
            FuelAmountLeft = fuelCapacity;
            CurrentPlace = 0;
            CurrentSpeed = 0;
            NumOfPassengers = numOfPass;
            mTrackLength = length;
            mRoundNumber = 0;
        }

        private void CheckFuel()
        {
            FuelAmountLeft -= CurrentSpeed;
            if (FuelAmountLeft <= 0)
            {
                GasStation.Instance.Refueling(this);
            }
        }

        private void ChangeSpeed()
        {
            if (CurrentSpeed + mAcceleration < mMaxSpeed)
            {
                CurrentSpeed += mAcceleration;
            }
            else
            {
                CurrentSpeed = mMaxSpeed;
            }
        }

        public void Drive()
        {
            while (NumOfPassengers > 0)
            {
                if (CurrentPlace >= mTrackLength)
                {
                    mRoundNumber++;
                    NumOfPassengers -= Capacity;
                    CurrentSpeed = 0;
                    CurrentPlace = 0;
                }
                else
                {
                    CheckFuel();
                    Console.WriteLine($"car {Name} ," +
                                      $" current speed : {CurrentSpeed}," +
                                      $" current place : {CurrentPlace}," +
                                      $" distance to end of the current round : {mTrackLength-CurrentPlace}" +
                                      $" number of rounds : {mRoundNumber}");
                    ChangeSpeed();
                    CurrentPlace += CurrentSpeed;
                    Thread.Sleep(1000);
                }
            }
            if (Winner=="")
            {
                Winner = Name;
                Console.WriteLine($"the car {Name} done the race");
            }    
        }
    }
}
