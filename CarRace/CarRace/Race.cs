﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace CarRace
{
    class Race
    {
        private List<RaceCar> mCars;


        public Race()
        {
            mCars = new List<RaceCar>();
        }

        public void GetCarsDetails()
        {
            Console.WriteLine("please enter the track length");
            int length = int.Parse(Console.ReadLine());
            Console.WriteLine("please enter the number of cars in the race");
            int numOfCars = int.Parse(Console.ReadLine());
            for (int i = 0; i < numOfCars; i++)
            {
                Console.WriteLine($"enter the {i}'s car details");
                Console.WriteLine("car name -");
                string carName = Console.ReadLine();
                Console.WriteLine("car max speed -");
                int maxSpeed = int.Parse(Console.ReadLine());
                Console.WriteLine("car acceleration -");
                int acceleration = int.Parse(Console.ReadLine());
                Console.WriteLine("car capacity -");
                int capacity = int.Parse(Console.ReadLine());
                Console.WriteLine("car fuel capacity -");
                int maxFuelCapacity = int.Parse(Console.ReadLine());


                Console.WriteLine("num of passengers -");
                int numOfPassengers = int.Parse(Console.ReadLine());
                RaceCar car = new RaceCar(carName, maxSpeed, acceleration, capacity, maxFuelCapacity, maxFuelCapacity, numOfPassengers, length);
                mCars.Add(car);
            }
        }

        public void ActivateRace()
        {
            Task[] tasks = new Task[mCars.Count];
            int x = 0;
            foreach (RaceCar car in mCars)
            {
                Task carTask = new Task(() => car.Drive());
                tasks[x] = carTask;
                x++;
                carTask.Start();
            }

            Task.WaitAll(tasks);
            Console.WriteLine($"the winner is {RaceCar.Winner}");
        }
    }
}
