using System;
using System.Collections.Generic;
using System.Text;

namespace CarRace
{
    class GasStation
    {
        private object refuelingLock;

        private static GasStation mInstance = null;

        private GasStation() 
        {
            refuelingLock = new object();
        }

        public static GasStation Instance
        {
            get
            {
                if (mInstance == null)
                {
                    mInstance = new GasStation();
                }
                return mInstance;
            }
        }

        public void Refueling(RaceCar car)
        {
            lock (refuelingLock)
            {
                Console.WriteLine($"{car.Name} currently refueing");
                car.FuelAmountLeft = car.MaxFuelCapacity;
            }
        }
    }
}
