﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CarRace
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Race race = new Race();
            race.GetCarsDetails();
            race.ActivateRace();
        }
    }
}
